#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
------------------------------------------------------------------------------
Modulo: formas
Motivo: Ejemplo de programacion orientada a objetos
------------------------------------------------------------------------------
Paradigmas de la Programacion
Prof. Lic. Carlos Zayas Guggiari <mail@carloszayas.com>
Licenciatura en Ciencias Informaticas
Facultad Politecnica - UNA
------------------------------------------------------------------------------
Formas geometricas basadas en el modulo 'tortuga'.
------------------------------------------------------------------------------
'''
#-----------------------------------------------------------------------------
# Importacion de modulos
#-----------------------------------------------------------------------------

from tortuga import *

#-----------------------------------------------------------------------------
# Definicion de constantes y variables
#-----------------------------------------------------------------------------

script = __name__ == '__main__' # Determina si se esta ejecutando como script

#-----------------------------------------------------------------------------
# Declaracion de funciones
#-----------------------------------------------------------------------------

def pacman(tortuga,tamano,color="yellow"):
    colorlapiz = tortuga.colorlapiz()
    colorrelleno = tortuga.colorrelleno()
    tortuga.colorlapiz(color)
    tortuga.colorrelleno(color)
    tortuga.iniciorelleno()
    tortuga.izquierda(45)
    tortuga.adelante(tamano)
    tortuga.izquierda(90)
    tortuga.circulo(tamano,270)
    tortuga.izquierda(90)
    tortuga.adelante(tamano)
    tortuga.derecha(135)
    tortuga.finrelleno()
    tortuga.alzar()
    tortuga.adelante(tamano)
    tortuga.bajar()
    tortuga.colorlapiz(colorlapiz)
    tortuga.colorrelleno(colorrelleno)

#-----------------------------------------------------------------------------

def ojo(tortuga,tamano):
    colorlapiz = tortuga.colorlapiz()
    colorrelleno = tortuga.colorrelleno()
    tortuga.bajar()
    tortuga.colorlapiz("white")
    tortuga.colorrelleno("white")
    tortuga.iniciorelleno()
    tortuga.circulo(tamano)
    tortuga.finrelleno()
    tortuga.colorlapiz("black")
    tortuga.colorrelleno("black")
    tortuga.iniciorelleno()
    tortuga.circulo(tamano/2)
    tortuga.finrelleno()
    tortuga.colorlapiz(colorlapiz)
    tortuga.colorrelleno(colorrelleno)

#-----------------------------------------------------------------------------

def fantasma(tortuga,tamano,color="pink"):
    colorlapiz = tortuga.colorlapiz()
    colorrelleno = tortuga.colorrelleno()
    posicion = tortuga.posicion()
    tortuga.bajar()
    tortuga.colorlapiz(color)
    tortuga.colorrelleno(color)
    tortuga.iniciorelleno()
    tortuga.izquierda(90)
    tortuga.adelante(tamano)
    tortuga.circulo(-tamano,180)
    tortuga.adelante(tamano)
    pliegue = (tamano-(tamano*0.09))/10
    for pliegues in repetir(5):
        tortuga.circulo(-pliegue,180)
        tortuga.circulo(pliegue,180)
    tortuga.circulo(-pliegue,180)
    tortuga.finrelleno()
    tortuga.adelante(tamano)
    for veces in repetir(2):
        tortuga.derecha(90)
        tortuga.alzar()
        tortuga.adelante(tamano/2)
        tortuga.izquierda(90)
        ojo(tortuga,-tamano/5)
    tortuga.colorlapiz(colorlapiz)
    tortuga.colorrelleno(colorrelleno)
    tortuga.alzar()
    tortuga.ubicar(posicion)
    tortuga.bajar()

#-----------------------------------------------------------------------------

def cuadrado(tortuga,lado,lleno=False):
    tortuga.bajar()
    if lleno: tortuga.iniciorelleno()
    for paso in repetir(4):
        tortuga.adelante(lado)
        tortuga.derecha(90)
    if lleno: tortuga.finrelleno()

#-----------------------------------------------------------------------------

def cuadrado_centrado(tortuga,lado,lleno=False):
    tortuga.alzar()
    tortuga.atras(lado/2)
    tortuga.izquierda(90)
    tortuga.atras(lado/2)
    cuadrado(tortuga,lado,lleno)
    tortuga.alzar()
    tortuga.adelante(lado/2)
    tortuga.derecha(90)
    tortuga.adelante(lado/2)
    tortuga.bajar()

#-----------------------------------------------------------------------------

def poligono(tortuga,lado,lados=3,lleno=False):
    tortuga.bajar()
    if lleno: tortuga.iniciorelleno()
    for paso in repetir(lados):
        tortuga.adelante(lado)
        tortuga.derecha(360/lados)
    if lleno: tortuga.finrelleno()

#-----------------------------------------------------------------------------

def formas_main():

    'Funcion principal.'

    v = Ventana(800,600)
    v.colorfondo("black")

    t = Tortuga()
    t.colorlapiz("white")
    t.colorrelleno("white")
    t.forma("turtle")
    t.velocidad("fastest")
    t.ocultar()

    pacman(t,80)
    cuadrado_centrado(t,40,True)
    t.alzar(); t.adelante(100)
    fantasma(t,80)
    t.alzar(); t.mirar(0); t.ubicar(200,-150)
    fantasma(t,60,"cyan")
    t.alzar(); t.mirar(0); t.ubicar(-300,100)
    poligono(t,100)

    for lados in [3,4,5,6,7,8]:
        poligono(t,100,lados)

    v.pausa()

#-----------------------------------------------------------------------------

if script: formas_main() # Si se ejecuta como script...