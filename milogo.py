#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
------------------------------------------------------------------------------
Script: milogo 0.9 - Python 2.7.3
Motivo: Ejemplo de programacion orientada a objetos
------------------------------------------------------------------------------
Paradigmas de la Programacion
Prof. Lic. Carlos Zayas Guggiari <mail@carloszayas.com>
Licenciatura en Ciencias Informaticas
Facultad Politecnica - UNA
------------------------------------------------------------------------------
MiLogo es un mini interprete Logo basado en el modulo 'tortuga'.
------------------------------------------------------------------------------
'''
#-----------------------------------------------------------------------------
# Importacion de modulos
#-----------------------------------------------------------------------------

import tortuga    # "Castellanizacion" de clases basadas en el modulo turtle
import readline   # Aumenta la funcionalidad de la funcion raw_input
import sys        # Acceso a variables del interprete
import os         # Funcionalidades del sistema operativo
import os.path    # Manejo de trayectorias de archivos
import subprocess # Ejecucion y analisis de subprocesos

#-----------------------------------------------------------------------------
# Definicion de funciones de apoyo
#-----------------------------------------------------------------------------

readline.parse_and_bind("tab: complete")

def completador(texto, estado):

    resultados = [x for x in comandos if x.startswith(texto)] + [None]

    return resultados[estado]

readline.set_completer(completador)

#-----------------------------------------------------------------------------

def leer(archivo):

    'Retorna una lista con el contenido de un archivo de programa en MiLogo.'

    lista = open(archivo).readlines() if os.path.exists(archivo) else []

    lista = [item.split(';')[0].strip() for item in lista] # Quita comentarios

    lista = [item for item in lista if item != ''] # Elimina lineas vacias

    return lista

#------------------------------------------------------------------------------

def columnas(lista, cols=2, padding=2):

    'Retorna una lista con los elementos encolumnados.'

    ancho = max(len(item) for item in lista) + padding

    lineas = []

    for i in xrange(0,len(lista),cols):
        linea = ''
        for item in sorted(lista)[i:i+cols]:
            linea += item.ljust(ancho)
        lineas.append(linea)

    return '\n'.join(lineas)

#------------------------------------------------------------------------------

def mins(lista):

    'Retorna una lista con los elementos convertidos en minusculas.'

    return [str(item).lower() for item in lista]

#-----------------------------------------------------------------------------

def reservadas():

    'Retorna una lista de las palabras reservadas.'

    return comandos + mins(procedimientos.keys()) + mins(variables.keys())

#-----------------------------------------------------------------------------

def aviso(cadena):

    'Imprime una cadena. Funcion que centraliza todas las llamadas a "print".'

    print cadena

#-----------------------------------------------------------------------------

def pedir(mensaje='', precargado=''):

    'Alternativa a raw_input que agrega un historial y un valor precargado.'

    readline.set_startup_hook(lambda:readline.insert_text(precargado))

    try:
        return raw_input(mensaje)
    finally:
        readline.set_startup_hook()

#-----------------------------------------------------------------------------
# Captura de argumentos de la linea de comandos
#-----------------------------------------------------------------------------

nombre     = ''      # Nombre de archivo con sentencias en MiLogo
ext        = '.mil'  # Extension de archivo de programa en MiLogo
ancho,alto = 640,480 # Dimensiones de la ventana para la tortuga

args = sys.argv[1:] # Se ignora el nombre del programa (indice 0)

for arg in args:
    if os.path.exists(arg):
        nombre = arg
        if not nombre.endswith(ext): nombre += ext
    else:
        res = arg.split('x')
        if len(res) == 2:
            if res[0].isdigit(): ancho = int(res[0])
            if res[1].isdigit(): alto  = int(res[1])
        else:
            aviso(arg + '?')

#-----------------------------------------------------------------------------
# Definicion de funciones MiLogo
#-----------------------------------------------------------------------------

def para(args, lineas):

    'Procedimiento con argumentos opcionales.'

    if args:

        proc = args.pop(0)

        if proc not in reservadas():

            if type(proc) is str:
                blq, lineas = bloque(lineas, proc)
                procedimientos[proc] = [args, blq]
            else:
                aviso(error(str(proc)+' en "para"'))

        else:

            aviso('No se puede usar "{}" como nombre'.format(proc))

    else:

        aviso(error('"para" sin un nombre'))

    return lineas

#-----------------------------------------------------------------------------

def repetir(args, lineas):

    'Repetir bloque de sentencias.'

    if args:

        if len(args) == 1:

            if type(args[0]) == int:

                blq, lineas = bloque(lineas)

                veces = tortuga.repetir(args[0])

                for vez in veces: blk, sigue = pila(blq)

            else:

                aviso(error('"repetir" con "{}"'.format(args[0])))

        else:

            aviso(error('"repetir" con tantos datos'))

    else:

        aviso(error('"repetir" sin una cantidad'))

    return lineas

#-----------------------------------------------------------------------------

def hacer(programa):

    'Ejecuta un archivo de programa.'

    archivo = '' if not programa else programa[0]

    salida = ''

    if archivo and not archivo.endswith(ext): archivo += ext

    lista = [] if not archivo else leer(archivo)

    if lista:
        print "Hecho!"
    else:
        for item in os.listdir('.'):
            if item.endswith(ext): salida += item[:-len(ext)] + '\n'

    return salida.strip()

#-----------------------------------------------------------------------------

def guardar(archivo):

    'Guarda en un archivo los procedimientos y las variables en memoria.'

    archivo = '' if not archivo else archivo[0]

    return 'Ambiente guardado en' + archivo

#-----------------------------------------------------------------------------

def consola(cmd):

    'Retorna la salida de un comando de consola.'

    salida = ''

    if cmd:

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                      stderr=subprocess.STDOUT)

            salida = ''.join(p.stdout.readlines())
        except:
            aviso(error('"'+cmd[0]+'"'))

    else:

        pass # pendiente

    return salida.strip()

#-----------------------------------------------------------------------------

def ayuda(args=None):

    'Retorna texto de ayuda general o sobre un comando específico.'

    if args:
        texto = help(getattr(__name__,args[0]))
    else:
        texto = '[Tortuga]\n' + columnas(ordenes.keys(),4) + '\n' + \
                '[Otros]\n' + columnas(milogo.keys())

    return texto

#-----------------------------------------------------------------------------

def nada(arg=None):

    'Funcion vacia para ordenes "salir" y "fin" que se atienden en "pila".'

    pass

#-----------------------------------------------------------------------------
# Definicion de variables globales
#-----------------------------------------------------------------------------

script = __name__ == '__main__' # Determina si se esta ejecutando como script

v = tortuga.Ventana(ancho,alto)
t = tortuga.Tortuga()

ordenes = {
            'adelante'      : t.adelante,
            'atras'         : t.atras,
            'derecha'       : t.derecha,
            'izquierda'     : t.izquierda,
            'ubicar'        : t.ubicar,
            'ubicarx'       : t.ubicarx,
            'ubicary'       : t.ubicary,
            'mirar'         : t.mirar,
            'casa'          : t.casa,
            'circulo'       : t.circulo,
            'punto'         : t.punto,
            'sello'         : t.sello,
            'borrarsello'   : t.borrarsello,
            'borrarsellos'  : t.borrarsellos,
            'deshacer'      : t.deshacer,
            'velocidad'     : t.velocidad,
            'posicion'      : t.posicion,
            'hacia'         : t.hacia,
            'x'             : t.x,
            'y'             : t.y,
            'grados'        : t.grados,
            'distancia'     : t.distancia,
            'bajar'         : t.bajar,
            'alzar'         : t.alzar,
            'grosor'        : t.grosor,
            'estabajada'    : t.estabajada,
            'colorlapiz'    : t.colorlapiz,
            'colorrelleno'  : t.colorrelleno,
            'iniciorelleno' : t.iniciorelleno,
            'finrelleno'    : t.finrelleno,
            'reiniciar'     : t.reiniciar,
            'limpiar'       : t.limpiar,
            'escribir'      : t.escribir,
            'ocultar'       : t.ocultar,
            'mostrar'       : t.mostrar,
            'esvisible'     : t.esvisible,
            'forma'         : t.forma,

            'titulo'        : v.titulo,
            'colorfondo'    : v.colorfondo,
            'imagenfondo'   : v.imagenfondo,
            'pausa'         : v.pausa,
            'cerrar'        : v.cerrar
          }

milogo =  {
            'para'          : para,
            'repetir'       : repetir,
            'hacer'         : hacer,
            'guardar'       : guardar,
            'consola'       : consola,
            'ayuda'         : ayuda,
            'salir'         : nada,
            'fin'           : nada
          }

comandos = ordenes.keys(); comandos.extend(milogo.keys()); comandos.sort()

procedimientos = {}

variables = {}

#-----------------------------------------------------------------------------
# Definicion de funciones principales
#-----------------------------------------------------------------------------

def reemplazar(lista):

    'Reemplaza las variables por sus valores.'

    for indice in range(len(lista)):
        if type(lista[indice]) is str:
            if lista[indice].isdigit():
                lista[indice] = int(lista[indice])
            else:
                if lista[indice].lower() in mins(variables.keys()):
                    lista[indice] = variables[lista[indice]]

    return lista

#-----------------------------------------------------------------------------

def analizar(linea):

    'Analisis de linea.'

    orden, args = '', []

    l = linea.split('=')

    if len(l) > 1:

        clave = l[0].lower().strip()
        valor = l[1].strip() if not l[1].isdigit() else int(l[1])

        variables[clave] = reemplazar([valor])[0]

    else:

        l = linea.split()
        c = l.pop(0).lower()

        if c in comandos:
            orden, args = c, reemplazar(l)
        elif c in mins(procedimientos.keys()):
            #-----------------------------------------------------------------
            # Pendiente: Analizar y procesar argumentos de procedimiento
            #-----------------------------------------------------------------
            args = reemplazar(l)
            if len(args) == len(procedimientos[c][0]):
                for i in range(len(args)):
                    variables[procedimientos[c][0][i]] = args[i]
                lineas, sigue = pila(procedimientos[c][1])
            else:
                aviso(error('"'+c+'" sin datos correctos'))
        elif c in mins(variables.keys()):
            aviso(variables[c])
        else:
            aviso(error('"'+c+'"'))

    return orden, args

#-----------------------------------------------------------------------------

def ejecutar(orden, args=[]):

    'Ejecucion de linea.'

    try:
        salida = ordenes.get(orden, error)(*args)
    except:
        salida = "Error: " + str(sys.exc_info()[1]) # Mensaje de error

    return salida

#-----------------------------------------------------------------------------

def bloque(lineas, etiqueta=''):

    'Gestiona un bloque de sentencias (para, repetir).'

    blq = []
    lns = lineas[:]

    final = 'fin '+etiqueta if etiqueta else 'fin'

    if lineas:

        for indice in range(len(lineas)):
            if lineas[indice].strip().lower() == final:
                blq = lineas[:indice]
                lns = lineas[indice+1:]
                break

    else:

        fin = False

        while not fin:
            linea = pedir('. ').strip()
            if linea:
                fin = linea.lower() == final
                if not fin: blq.append(linea)

    return blq, lns

#-----------------------------------------------------------------------------

def pila(lineas):

    'Ejecuta una pila de sentencias.'

    sigue = True

    lineas = lineas[:]

    while lineas:

        linea = lineas.pop(0).split(';')[0].strip() # comentario = punto y coma

        if linea:

            orden, args = analizar(linea)

            if orden:

                if orden == 'salir':
                    sigue = False
                    break
                elif orden == 'fin':
                    aviso(error('"fin" sin "para" o "repetir"'))
                elif orden in ['hacer','guardar','consola','ayuda']:
                    x = milogo.get(orden)(args)
                    if x: aviso(x)
                elif orden in milogo.keys():
                    lineas = milogo.get(orden)(args, lineas)
                else:
                    salida = ejecutar(orden, args)
                    if salida: aviso(salida)

    return lineas, sigue

#-----------------------------------------------------------------------------

def error(mensaje=None):

    'Mensaje de error.'

    return 'No se que hacer con ' + mensaje if mensaje else None

#-----------------------------------------------------------------------------
# Función principal
#-----------------------------------------------------------------------------

def main(mensaje, cursor, lineas):

    'Funcion principal.'

    ciclo = True

    if mensaje: aviso(mensaje)

    while ciclo:

        if not lineas: lineas = [pedir(cursor)]

        lineas, ciclo = pila(lineas)

#-----------------------------------------------------------------------------
# Bloque principal
#-----------------------------------------------------------------------------

programa = [] if not nombre else leer(nombre)

if script: main('Bienvenido a MiLogo!', '> ', programa)
