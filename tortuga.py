#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
------------------------------------------------------------------------------
Modulo: tortuga
Motivo: Ejemplo de programacion orientada a objetos
------------------------------------------------------------------------------
Paradigmas de la Programacion
Prof. Lic. Carlos Zayas Guggiari <mail@carloszayas.com>
Licenciatura en Ciencias Informaticas
Facultad Politecnica - UNA
------------------------------------------------------------------------------
"Castellanizacion" de clases basadas en el modulo turtle.
------------------------------------------------------------------------------
http://docs.python.org/library/turtle.html
http://www.tcl.tk/man/tcl8.5/TkCmd/colors.htm
'''
#-----------------------------------------------------------------------------
# Importacion de modulos
#-----------------------------------------------------------------------------

from turtle import *

#-----------------------------------------------------------------------------
# Definicion de constantes y variables
#-----------------------------------------------------------------------------

script = __name__ == '__main__' # Determina si se esta ejecutando como script

normal = ("Arial", 8, "normal") # Preferencias predeterminadas para el texto

#-----------------------------------------------------------------------------
# Declaracion de clases
#-----------------------------------------------------------------------------

class Tortuga:

    'Graficos Tortuga.'

    def __init__(self):                self.tortuga = Turtle()

    # -----------------------------------------------------------------------
    # Movimiento de la tortuga
    # -----------------------------------------------------------------------

    def adelante(self,pasos):          self.tortuga.forward(pasos)

    def atras(self,pasos):             self.tortuga.backward(pasos)

    def derecha(self,grados):          self.tortuga.right(grados)

    def izquierda(self,grados):        self.tortuga.left(grados)

    def ubicar(self,x,y=None):         self.tortuga.setpos(x,y)

    def ubicarx(self,x):               self.tortuga.setx(x)

    def ubicary(self,y):               self.tortuga.sety(y)

    def mirar(self,grados):            self.tortuga.setheading(grados)

    def casa(self):                    self.tortuga.home()

    def circulo(self,r,e=None,s=None): self.tortuga.circle(r,e,s)

    def punto(self,tam=None,*color):   self.tortuga.dot(tam,*color)

    def sello(self):                   return self.tortuga.stamp()

    def borrarsello(self,sello):       self.tortuga.clearstamp(sello)

    def borrarsellos(self,n=None):     self.tortuga.clearstamps(n)

    def deshacer(self):                self.tortuga.undo()

    def velocidad(self,valor=None):    self.tortuga.speed(valor)

    # -----------------------------------------------------------------------
    # Consultar ubicacion de la tortuga
    # -----------------------------------------------------------------------

    def posicion(self):                return self.tortuga.position()

    def hacia(self,x,y=None):          return self.tortuga.towards(x,y)

    def x(self):                       return self.tortuga.xcor()

    def y(self):                       return self.tortuga.ycor()

    def grados(self):                  return self.tortuga.heading()

    def distancia(self,x,y=None):      return self.tortuga.distance(x,y)

    # -----------------------------------------------------------------------
    # Control del lapiz
    # -----------------------------------------------------------------------

    def bajar(self):                   self.tortuga.pendown()

    def alzar(self):                   self.tortuga.penup()

    def grosor(self,ancho=None):       self.tortuga.pensize(ancho)

    def estabajada(self):              return self.tortuga.isdown()

    def colorlapiz(self,*args):        return self.tortuga.pencolor(*args)

    def colorrelleno(self,*args):      return self.tortuga.fillcolor(*args)

    def iniciorelleno(self):           self.tortuga.begin_fill()

    def finrelleno(self):              self.tortuga.end_fill()

    def reiniciar(self):               self.tortuga.reset()

    def limpiar(self):                 self.tortuga.clear()

    def escribir(self,
                 cadena,
                 mover   = False,
                 alinear = "left",
                 fuente  = normal):
        self.tortuga.write(cadena,mover,alinear,fuente)

    # -----------------------------------------------------------------------
    # Estado de la tortuga
    # -----------------------------------------------------------------------

    def ocultar(self):                 self.tortuga.hideturtle()

    def mostrar(self):                 self.tortuga.showturtle()

    def esvisible(self):               return self.tortuga.isvisible()

    def forma(self,nombre=None):       self.tortuga.shape(nombre)

#-----------------------------------------------------------------------------

class Ventana:

    'Ventana para graficos Tortuga.'

    def __init__(self,x,y,w=0,h=0): self.ventana = Screen(); setup(x,y,w,h)

    def titulo(self,cadena):        self.ventana.title(cadena)

    def colorfondo(self,*color):    self.ventana.bgcolor(*color)

    def imagenfondo(self,img=None): return self.ventana.bgpic(img)

    def pausa(self):                self.ventana.exitonclick()

    def cerrar(self):               self.ventana.bye()

#-----------------------------------------------------------------------------
# Declaracion de funciones
#-----------------------------------------------------------------------------

def repetir(veces): return range(veces+1)[1:]

#-----------------------------------------------------------------------------

def tortuga_main():

    'Funcion principal.'

    pass

#-----------------------------------------------------------------------------

if script: tortuga_main() # Si se ejecuta como script...