#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
------------------------------------------------------------------------------
Script: tortuga-demo
Motivo: Ejemplo de programacion orientada a objetos
------------------------------------------------------------------------------
Paradigmas de la Programacion
Prof. Lic. Carlos Zayas Guggiari <mail@carloszayas.com>
Licenciatura en Ciencias Informaticas
Facultad Politecnica - UNA
------------------------------------------------------------------------------
Demostracion del modulo 'tortuga'.
------------------------------------------------------------------------------
'''
#-----------------------------------------------------------------------------
# Importacion de modulos
#-----------------------------------------------------------------------------

from tortuga import Tortuga, Ventana, repetir

script = __name__ == '__main__' # Determina si se esta ejecutando como script

#-----------------------------------------------------------------------------
# Función principal
#-----------------------------------------------------------------------------

def main():

    'Funcion principal.'

    v = Ventana(800,600)
    v.titulo("Prueba")
    v.colorfondo("lightgreen")
    v.imagenfondo("campo.gif")

    t0 = Tortuga()
    t0.ocultar()
    t0.alzar()
    t0.ubicar(-100,150)
    t0.colorlapiz("brown")
    t0.escribir("Este es un ejemplo del modulo 'tortuga'")

    t1 = Tortuga()
    t1.colorlapiz("red")
    t1.ocultar()
    t1.velocidad(0)

    for a in repetir(60):
        for b in repetir(4):
            t1.adelante(50)
            t1.derecha(90)
        t1.derecha(6)

    t2 = Tortuga()
    t2.colorlapiz("blue")
    t2.ocultar()
    t2.velocidad(0)
    t2.alzar()
    t2.ubicar(200,200)
    t2.bajar()

    for a in repetir(60):
        for b in repetir(4):
            t2.adelante(50)
            t2.derecha(90)
        t2.derecha(6)

    t0.casa()
    t0.forma("turtle")
    t0.mostrar()
    t0.colorlapiz("green")
    t0.escribir("LOGO Lives!",True,"center",("Helvetica",30,"bold"))

    v.pausa()

#-----------------------------------------------------------------------------
# Bloque principal
#-----------------------------------------------------------------------------

if script: main() # Si se ejecuta como script, inicia la funcion principal