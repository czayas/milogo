#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
------------------------------------------------------------------------------
Script: formas-demo
Motivo: Ejemplo de programacion orientada a objetos
------------------------------------------------------------------------------
Paradigmas de la Programacion
Prof. Lic. Carlos Zayas Guggiari <mail@carloszayas.com>
Licenciatura en Ciencias Informaticas
Facultad Politecnica - UNA
------------------------------------------------------------------------------
Ejemplo de uso del modulo 'formas'.
------------------------------------------------------------------------------
'''
#-----------------------------------------------------------------------------
# Importacion de modulos
#-----------------------------------------------------------------------------

from formas import *

#-----------------------------------------------------------------------------
# Definicion de constantes y variables
#-----------------------------------------------------------------------------

script = __name__ == '__main__' # Determina si se esta ejecutando como script

#-----------------------------------------------------------------------------

def main():

    'Funcion principal.'

    v = Ventana(800,600)
    v.colorfondo("black")

    t = Tortuga()
    t.colorlapiz("white")
    t.colorrelleno("white")
    t.forma("turtle")
    t.velocidad("fastest")
    t.ocultar()

    for veces in repetir(4):
        fantasma(t,40);t.mirar(0);t.alzar();t.adelante(90)

    t.casa()
    t.ubicary(-100)

    for veces in repetir(4):
        pacman(t,40);t.alzar();t.adelante(60)

    v.pausa()

#-----------------------------------------------------------------------------

if script: main() # Si se ejecuta como script, inicia la funcion principal